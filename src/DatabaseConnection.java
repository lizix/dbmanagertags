package TBTagsManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;


public class DatabaseConnection {

	 //Instance variables                                                    
    //Username                                                              
    private static final String USERNAME = "root";
    //Password                                                              
    private static final String PASSWORD = "root";
    //Domain                                                                
    private static final String DOMAIN = "jdbc:mysql://localhost:8889/tangenbox";

    public Connection connection = null;

    public static final DatabaseConnection sharedInstance = new DatabaseConnection();
    
    //Constructor                                                           
    public DatabaseConnection() {

            try {

                    System.out.println("Connecting...");
                    this.connection = DriverManager.getConnection(DOMAIN, USERNAME, PASSWORD);
                    System.out.println("Connected");

            } catch (SQLException e) {
            		System.out.println("connecting failed");
                    //Write to database error log.                          

            }

    }
    

    /**                                                                     
     * Songleton method that return an instance of itself                   
     * @return Database                                                     
     */
    public static DatabaseConnection getInstance() {

            return sharedInstance;

    }

    /**                                                                     
     * Get connection to database                                           
     * @return                                                              
     */
    public Connection getConnection() {

            return this.connection;

    }
	
}
