package TBTagsManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * this file manages tag tables in database
 * @author Roy Li
 * TangenBox. All rights reserved.
 *
 */

public class TBTagsManager {
	
	private DatabaseConnection db_connection;
	private PreparedStatement insertTagStatement;
	private PreparedStatement insertScheduleStatement;
	
	
	private static final String insertScheduleQuery = "INSERT INTO schedules (name) VALUES ('schedule')";

	/**
	 * constructor
	 */
	public TBTagsManager(String tagIndex) {
		
		this.db_connection = DatabaseConnection.getInstance();
		try {
			
			this.insertScheduleStatement = this.db_connection.getConnection().prepareStatement(insertScheduleQuery,  Statement.RETURN_GENERATED_KEYS);
			
			//construct insert tag query
			String insertTagQuery = "INSERT INTO tag" + tagIndex + " (scheduleId) VALUES (?) ";
			this.insertTagStatement = this.db_connection.getConnection().prepareStatement(insertTagQuery);
			
		} catch (SQLException e) {
			
			e.printStackTrace();
			System.out.println("Database connection failed");
		}
				
	}
	
	/**
	 * insert x schedules into tagX
	 *@param int numOfSchedules
	 *@param int indexOfTag
	 *@return void
	 */
	private void insertSchedules(int numOfSchedules) {
		
		for (int i = 0; i < numOfSchedules; i++) {
						
			try {
				
				//id of inserted schedule
				int scheduleId = -1;
				
				//insert schedule
				this.insertScheduleStatement.executeUpdate();
				ResultSet resultSet = this.insertScheduleStatement.getGeneratedKeys();
				
				if (resultSet != null && resultSet.next()) {
					
					scheduleId = resultSet.getInt(1);
					
				}
				
				//insert tag
				this.insertTagStatement.setString(1, Integer.toString(scheduleId));
				
				this.insertTagStatement.execute();
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("SQL insertion has failed, make sure you have the right tay table in database");
				System.out.println("Quiting...");
				System.exit(1);
				
			}
		
			
		}
		
	}
	
	
	
	public static void main(String[] args) {
		
		if (args.length < 2) {
			
			System.out.println("Usage: Java TBTagsManager <number of schedules to insert> <index of tag schedules are to be inserted to>");
			System.exit(1);
		}
		
		try {
			
			//number of schedules to be inserted
			int numOfSchedules = Integer.parseInt(args[0]);
			String indexOfTag = args[1];
			
			TBTagsManager tbManager = new TBTagsManager(indexOfTag);
			tbManager.insertSchedules(numOfSchedules);
			
			System.out.printf("Inserted %s schedules into tag%s. \n", args[0], args[1]);
						
		} catch (Exception e) {
			
			System.out.println("Exception here");
			System.out.println("Arguments are illegal!");
			System.out.println("Usage: Java TBTagsManager <number of schedules to insert> <index of tag schedules are to be inserted to>");
			System.exit(1);
		}
		
		
		
		
	}
	
	
}
